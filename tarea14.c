#include <stdio.h>
#include "funcionT.h"
#include "funcionI.h"
#include "funcionH.h"
#include "funcionE.h"
#include "funcionR.h"
#include "funcionY.h"

int main(int argc, char const *argv[]) {

  fnT();
  fnI();
  fnH();
  fnE();
  fnR();
  fnY();

  return 0;
}
